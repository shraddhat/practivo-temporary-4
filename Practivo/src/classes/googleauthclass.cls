public class googleauthclass{

public string authtoken{get;set;}
public string refereshtoken{get;set;}
public string bodyprint{get;set;}

//Settings needed on the google cloud console.One can store this securely in custom settings or an object.

public static final string CLIENT_SECRET='tE90X10DpeAEeA2bmPQntqgT';//Fill as per your registered app settings in google console
public static final string CLIENT_ID='919552163174-9fp84tq5uar3vb5lpd0l6f57fo3fkg97.apps.googleusercontent.com';//Fill as per your registered app settings in google console
public static final string REDIRECT_URL='https://c.ap2.visual.force.com/apex/GoogleAuth';

public static final string OAUTH_TOKEN_URL='https://accounts.google.com/o/oauth2/token';
public static final string OAUTH_CODE_END_POINT_URL='https://accounts.google.com/o/oauth2/auth';

public static final string GRANT_TYPE='grant_type=authorization_code';

//Scope URL as per oauth 2.0 guide of the google 
public static final string SCOPE='https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/calendar';
public static final string STATE='/profile';

//Approval Prompt Constant
public static final string APPROVAL_PROMPT='force';




   public pagereference connect(){
   
      Http h = new Http();
      HttpRequest req = new HttpRequest();
      req.setEndpoint('https://accounts.google.com/o/oauth2/v2/auth');
      req.setHeader('response_type','code');
      req.setHeader('client_id',EncodingUtil.urlEncode(CLIENT_ID,'UTF-8'));
      req.setHeader('redirect_uri',EncodingUtil.urlEncode(REDIRECT_URL,'UTF-8'));
      req.setHeader('scope',EncodingUtil.urlEncode(SCOPE,'UTF-8'));
      //req.setHeader('state','DarshanTheGreat');
      req.setHeader('access_type','offline');
      req.setHeader('prompt','select_account');
      //req.setHeader('login_hint',code);
      //req.setHeader('include_granted_scopes',code);
     req.setHeader('Content-Type','application/x-www-form-urlencoded');
     req.setMethod('POST');
     req.setHeader('Content-length','1024');
     req.setTimeout(60000);
     // HttpResponse res = h.send(req);
     // system.debug('body'+res.getbody());
     String x=OAUTH_CODE_END_POINT_URL+'?access_type=offline&scope='
     +EncodingUtil.urlEncode(SCOPE,' UTF-8')
     +'&state='+EncodingUtil.urlEncode(STATE,'UTF-8')
     +'&redirect_uri='+EncodingUtil.urlEncode(REDIRECT_URL,'UTF-8')
     +'&response_type=code&client_id='+CLIENT_ID
     +'&approval_prompt='+APPROVAL_PROMPT;
     
     pagereference p=new pagereference(x);
     return p;
     
   }
   
    public pagereference showtoken(){
   
   string codeparam = apexpages.currentpage().getparameters().get('code');
      
            // Instantiate a new http object
    Http h = new Http();
    
    String body= 'code='+codeparam
                +'&client_id='+CLIENT_ID
                +'&client_secret='+CLIENT_SECRET
                +'&redirect_uri='+REDIRECT_URL
                +'&'+GRANT_TYPE;
    
// Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
    HttpRequest req = new HttpRequest();
    req.setEndpoint(OAUTH_TOKEN_URL);
    req.setHeader('Content-Type','application/x-www-form-urlencoded');
    req.setMethod('POST');
    req.setBody(body);
    
    system.debug('REQUEST BODY'+body);

// Send the request, and return a response
    HttpResponse res = h.send(req);
    
    system.debug('body'+res.getbody());
    
    bodyprint=res.getbody();
    
    return null;
   
   }
   
  
}