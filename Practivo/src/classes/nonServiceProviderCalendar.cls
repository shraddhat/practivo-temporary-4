public class nonServiceProviderCalendar
{
    public list<availableMatrix> availableWeekCalendar;
    public map<Integer,availableMatrix> calendar; 
    public Integer noOfResourceRequired;
    public Integer totalNoOfResources;
    public Integer noOfresourceUsed;
    public Id ResourceTypeId;
    boolean removeValueFromMatrix=true;
    boolean addValueToMatrix=false;
    //this map stores resource type Vs count of resource and list of resource ONLY FOR SAVE LOGIC
    public map<Id,list<actualUsedResources>> resourcesUsedMap;
    boolean saveMapRequired =false;
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/  
    public nonServiceProviderCalendar()
    {
        calendar = new map<Integer,availableMatrix>();
    }
    public nonServiceProviderCalendar(boolean saveMap)
    {
        //system.debug('savemap values'+saveMap);
        calendar = new map<Integer,availableMatrix>();
        if(savemap==true)
        {
            //system.debug('yes we are saving this data');
            saveMapRequired = true;
            resourcesUsedMap =  new map<Id,list<actualUsedResources>>();
        }
            
    }
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/
    public void initialiseCalendarForNonSPResourceType(list<sObject> resourcesLst ,boolean isAssistant)
    { 
        integer overBookingLimit=0;
        list<contact> assistantsLst;
        list<Resources__c> nonHumanResourcesLst;
        if(isAssistant)
        {   
            assistantsLst = (list<Contact>)(resourcesLst);
            if(assistantsLst!=null)
            for(contact assistant : assistantsLst)
            {
                overBookingLimit = overBookingLimit + Integer.ValueOf(assistant.Over_Booking_Limit__c);
            }
        }
        else
        {
            nonHumanResourcesLst =  (list<Resources__c>) resourcesLst;
            if(nonHumanResourcesLst!=null)
            for(Resources__c nonHumanResource : nonHumanResourcesLst)
            {
                overBookingLimit = overBookingLimit + Integer.ValueOf(nonHumanResource.Over_Booking_Limit__c);
            }
        }
        //if(overBookingLimit==0)//values is not changed means no Values Required for that resource Type 
            //overBookingLimit=100; 
        this.totalNoOfResources = overBookingLimit;
        system.debug('-- No of resources here are '+this.totalNoOfResources+'   --ResourceTypeId --'+this.ResourceTypeId);
        for(integer itr=fetchCalendarUtils.forLoopStartIndex;itr<=fetchCalendarUtils.forLoopEndIndex;itr++) //for week initialse with all overbooking limit.
        {
            availableMatrix availableMatrixObj = new availableMatrix();
            availableMatrixObj.dayCode = itr;
            availableMatrixObj.timeSlotMatrix = Utils.initMatrix(availableMatrixObj.timeSlotMatrix,fetchCalendarUtils.dayStartTime,fetchCalendarUtils.dayEndTime,overBookingLimit);
            calendar.put(itr,availableMatrixObj);
        }
    }  
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/  
public void markCalendarUnavailableForResourceType (list<nonHumanResourceDataSet> dataSets, map<Id,map<Id,Integer>> actualBookedResourceMap,boolean isAssistant)
    {     
        system.debug('--markCalendarUnavailableForResourceType : dataSets ---'+dataSets);
        system.debug('--markCalendarUnavailableForResourceType : actualBookedResourceMap ---'+actualBookedResourceMap); //this is requirements
        Integer startTime,endTime,weekDay,removeValue=0,noOfResources=0;
        Boolean isValueChanged= false,allConditionTrue=false;
        set<Id> bookedAppTypsIds = actualBookedResourceMap.keySet();
        list<actualUsedResources> actualUsedResourcesLst = new list<actualUsedResources>();  
        //for(Id appointmentId : bookedAppTypsIds)
       
            //nonHumanResourceDataSet dataSet = getDataSet(dataSets,appointmentId);
            for(nonHumanResourceDataSet dataSet  : dataSets)
            {
                removeValue = 0;isValueChanged=false;
                system.debug('-- dataSet in loop --'+dataSet);
                for(Actual_Resource_Booked_Mapping__c ARBMaps : dataSet.bookedActualResource) 
                {
                   system.debug('(Actual_Resource_Booked_Mapping__c in loop'+ARBMaps);
                    noOfResources = 0;
                    if(dataSet.bookedResourceId == ARBMaps.Booked_Resources__c)
                    {
                        if(isAssistant  && ARBMaps.Assistants__r.Resource_Type__c == this.ResourceTypeId)
                        {
                            allConditionTrue =true;
                            if(fetchCalendarUtils.isFetchingForClass) 
                            {
                                noOfResources = Integer.ValueOf(ARBMaps.Assistants__r.Over_Booking_Limit__c);
                                removeValue = removeValue + Integer.ValueOf(ARBMaps.Assistants__r.Over_Booking_Limit__c);
                                system.debug('-- markNonHumanUnavailable : Assistant : FETCHING FOR CLASS : class appointment removeValue --'+removeValue);
                            }
                            else
                            {
                                if(ARBMaps.doNotOverbook__c) //class Appointments
                                {
                                    noOfResources = Integer.ValueOf(ARBMaps.Assistants__r.Over_Booking_Limit__c);
                                    removeValue  = removeValue + Integer.ValueOf(ARBMaps.Assistants__r.Over_Booking_Limit__c);
                                    system.debug('-- markNonHumanUnavailable : Assistant  : class appointment removeValue --'+removeValue+' for resource--' + ARBMaps.Assistants__c);
                                }
                                else //one on one appointments
                                {
                                    noOfResources = Integer.ValueOf(ARBMaps.No_Of_Instance_Used__c);
                                    removeValue = removeValue + Integer.ValueOf(ARBMaps.No_Of_Instance_Used__c);
                                    system.debug('-- markNonHumanUnavailable : Assistant :  one on one removeValue --'+removeValue + ' for resource  -- '+ARBMaps.Assistants__c);
                                }
                            }
                            
                        }
                        else if(!isAssistant  && ARBMaps.Resources__r.Resource_Type__c == this.ResourceTypeId)
                        {
                            allConditionTrue =true;
                            if(fetchCalendarUtils.isFetchingForClass)
                            {
                                noOfResources = Integer.ValueOf(ARBMaps.Resources__r.Over_Booking_Limit__c);
                                removeValue = removeValue + Integer.ValueOf(ARBMaps.Resources__r.Over_Booking_Limit__c);
                                system.debug('-- markNonHumanUnavailable : non Human Resource : FETCHING FOR CLASS : class appointment removeValue --'+removeValue);
                            }
                            else
                            {
                                if(ARBMaps.doNotOverbook__c) //class Appointments
                                {
                                    noOfResources = Integer.ValueOf(ARBMaps.Resources__r.Over_Booking_Limit__c);
                                    removeValue  = removeValue + Integer.ValueOf(ARBMaps.Resources__r.Over_Booking_Limit__c);
                                    system.debug('-- markNonHumanUnavailable : non Human Resource : class appointment removeValue --'+removeValue);
                                }
                                else //one on one appointments
                                {
                                    noOfResources = Integer.ValueOf(ARBMaps.No_Of_Instance_Used__c);
                                    removeValue = removeValue + Integer.ValueOf(ARBMaps.No_Of_Instance_Used__c);
                                    system.debug('-- markNonHumanUnavailable : non Human Resource : one on one removeValue --'+removeValue);
                                }
                            }
                        }
                        if(allConditionTrue)
                        {
                            isValueChanged = true;
                            list<DateTime> timings = dataSet.timings;
                            if(timings!=null && !timings.isEmpty()) 
                            {
                               weekDay = getWeekDay(timings.get(0));
                               startTime = getTimeAt(timings , 0);
                               endTime = getTimeAt(timings , 1); 
                            }
                            if(saveMapRequired)
                            {
                                //resourcesUsedMap
                                actualUsedResources actualUsedResourcesObj = new actualUsedResources();
                                actualUsedResourcesObj.dayCode=weekDay;
                                actualUsedResourcesObj.startTime =startTime;
                                actualUsedResourcesObj.endTime =endTime;
                                actualUsedResourcesObj.noOfResourceUsed = noOfResources;
                                if(isAssistant)
                                    actualUsedResourcesObj.resourceId = ARBMaps.Assistants__r.Id;
                                else
                                    actualUsedResourcesObj.resourceId = ARBMaps.Resources__r.Id;
                                actualUsedResourcesLst.add(actualUsedResourcesObj);
                            }
                            allConditionTrue =false;
                            if(calendar.containsKey(weekDay))
                             {
                                system.debug('-- remove value from map--'+removeValue+' Start time'+startTime +'endTime '+endTime + 'Resource Type'+this.ResourceTypeId);
                                availableMatrix availableMatrixObj = calendar.get(weekDay); //list 
                                availableMatrixObj.timeSlotMatrix = Utils.addRemoveValueFromMatrix(availableMatrixObj.timeSlotMatrix ,startTime,endTime,removeValue,removeValueFromMatrix);
                             }
                        }
                    }
                }
                if(saveMapRequired)
                    resourcesUsedMap.put(this.ResourceTypeId,actualUsedResourcesLst);
                if(isValueChanged)
                {
                     /*if(calendar.containsKey(weekDay))
                     {
                        system.debug('-- remove value from map--'+removeValue);
                        availableMatrix availableMatrixObj = calendar.get(weekDay); //list 
                        availableMatrixObj.timeSlotMatrix = Utils.addRemoveValueFromMatrix(availableMatrixObj.timeSlotMatrix ,startTime,endTime,removeValue,removeValueFromMatrix);
                     }*/
                } 
                    
        } 
 }            
        
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/  
    Id getAppointmentId(map<Id,map<Id,Integer>> actualBookedResourceMap,Id ResourceTypeId)
    {
        Set<Id> appointmentIds = actualBookedResourceMap.KeySet();
        for(Id appTypId : appointmentIds)
        {
            map<Id,Integer> resourceTypeMap = actualBookedResourceMap.get(appTypId);
            if(resourceTypeMap.containsKey(ResourceTypeId))         
                    return appTypId;
        }
        return null;
    }
/*----------------------------------------------------------------------------------------------------------------------------------------------- */ 
    nonHumanResourceDataSet getDataSet(list<nonHumanResourceDataSet> dataSets,Id appTypId)
    {
        for(nonHumanResourceDataSet dataSet : dataSets)
        {
            if(dataSet.appointmentTypeId == appTypId)
                return dataSet;
        }
        return null; 
    }
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/   
    public Integer getTimeAt(list<DateTime> timings , integer index)
    {
        DateTime dt = timings.get(index);
        if(index==0)
            return Utils.getMilitaryTime(dt.format('HH:mm'),false);
        else if(index==1)
            return Utils.getMilitaryTime(dt.format('HH:mm'),true);
        else
            return 0;   
    }
 /*----------------------------------------------------------------------------------------------------------------------------------------------- */ 
    public Integer getWeekDay(DateTime dt)
    {
        Date DateObj = dt.dateGMT();
        Time TimeObj = dt.Time();  
        DateTime Dt1= DateTime.newInstanceGMT(DateObj,TimeObj);
        if(dt.format('u')=='1')
            return 1;
        else if(dt.format('u')=='2')
            return 2;
        else if(dt.format('u')=='3')
            return 3;
        else if(dt.format('u')=='4')
            return 4;         
        else if(dt.format('u')=='5')
            return 5;
        else if(dt.format('u')=='6')
            return 6;
        else if(dt.format('u')=='7')
            return 7;
        else
            return 0;
    }
/*----------------------------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------------------------------*/
/*This is wrapper class that helps to have appointments with actual resorces used in those*/
public class nonHumanResourceDataSet 
{
    public Id bookedResourceId;
    public Id appointmentTypeId;
    public list<DateTime> timings;
    public list<Actual_Resource_Booked_Mapping__c> bookedActualResource;
    public nonHumanResourceDataSet(){}
    public nonHumanResourceDataSet(Id bookedResID,list<DateTime> timings,Id appTypId)
    {
        this.bookedResourceId = bookedResID;
        this.timings =timings;
        this.appointmentTypeId = appTypId;
    }    
}
        
public class actualUsedResources
{
    public Integer startTime;
    public Integer endTime;
    public Integer dayCode;
    public Integer noOfResourceUsed;
    public string resourceId;
    public actualUsedResources()
    {} 
}
/*--------------------------------------------------------------------------------------------------------------------------------------------------*/
}