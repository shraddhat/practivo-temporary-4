// In a separate class so that it can be used elsewhere
Global class emailHelper {

    public static void sendEmail(ID recipient, ID candidate) {
    
      //New instance of a single email message
     Messaging.SingleEmailMessage mail = 
                new Messaging.SingleEmailMessage();
                List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
     
       // Who you are sending the email to
       mail.setTargetObjectId(recipient);
        Id templateId;
        list<EmailTemplate> Listemailtemp= [SELECT ID, name from EmailTemplate WHERE DeveloperName = 'Send_Appointment_Calendar'];
       if(!Listemailtemp.isEmpty())
       {
        templateId=Listemailtemp[0].id;
       }
       // The email template ID used for the email
       //mail.setTemplateId('00X280000010ulh');
       mail.setTemplateId(templateId);
              
       mail.setWhatId(candidate);    
       mail.setBccSender(false);
       mail.setUseSignature(false);
       mail.setSenderDisplayName('ePractice Appointments');
       mail.setSaveAsActivity(false);  
        /*allmsg.add(mail);
Messaging.sendEmail(allmsg,false);*/
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
            system.debug('ssss');
          }
}

/*
emailHelper.sendEmail('00390000015G3yd', '0039000001aiWrl');
*/