@isTest
public class Test_downloadcalendar
{
  static testMethod void testgetCalendar()
  {
      Contact con =  new Contact();
        con.FirstName = 'Anil';
        con.LastName = 'Dutt';
        con.Email = 'anil@swiftsestup.com';
        insert con;
      
      Attachment att=new Attachment();
            att.Body=Blob.valueof('sample');
            att.ContentType=String.valueof('text/calendar');
            att.Name=String.valueof('meeting.ics');
            att.description='Ical feed';
            att.parentId=con.Id;
            insert att;
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
req.requestURI = 'http://testprac-developer-edition.ap2.force.com/services/apexrest/myCalendar';  
        req.addParameter('fileId', att.Id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res; 
   downloadCalendar.getCalendar();
      System.assertNotEquals(RestContext.response.responseBody,null);
      req.addParameter('fileId', '');
      req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res; 
   downloadCalendar.getCalendar();
  }
    static testMethod void testgetCalendarNegative()
  {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'http://testprac-developer-edition.ap2.force.com/services/apexrest/myCalendar';  
        req.addParameter('fileId', 'avavavavavaaava');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res; 
      try
      {
        downloadCalendar.getCalendar();
      }
      catch(exception e)
      {
          System.assertEquals('System.QueryException: List has no rows for assignment to SObject', e.getMessage());
          
      }
  }
}