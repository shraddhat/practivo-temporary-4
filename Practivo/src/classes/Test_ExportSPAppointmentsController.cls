@isTest
public class Test_ExportSPAppointmentsController 
{
    static testmethod void testgetappts()
    {
         Account acct = new Account( Name = 'Test Account' );
                        insert acct;
        Datetime Datetimenow=datetime.now();
        Datetime EndDt=Datetimenow.addhours(5);
        Date Startdatetocalendar=Date.newinstance(Datetimenow.year(),Datetimenow.month(),Datetimenow.Day());
        Date Enddatetocalendar=Startdatetocalendar.adddays(10);                
        Contact con =  new Contact();
        con.FirstName = 'Anil';
        con.AccountId=acct.id;
        con.LastName = 'Dutt';
        con.Email = 'anil@swiftsestup.com';
        con.Start_Date_To_Generate_Calendar__c=String.valueof(Startdatetocalendar);//'2016-09-01';
        con.End_Date_To_Generate_Calendar__c=String.valueof(Enddatetocalendar);//'2016-09-30';
        insert con;
        Contact con1 =  new Contact();
        con1.AccountId=acct.id;
        con1.FirstName = 'Sample';
        con1.LastName = 'Test';
        con1.Email = 'Test@Setupdata.com';
        insert con1;
      
        Location__c loc=new Location__c();
        loc.name='Test City';
        loc.Address__c='Test 20,test road';
        insert loc;
        
        Category__c cat=new Category__c();
        cat.Name='physio';
        insert cat;
        
        AppointmentType__c apt= new AppointmentType__c();
        apt.Name='app1';
        apt.Category__c=cat.id;
        apt.Price__c=100;
        apt.Maximum_Participant__c=1;
        insert apt;
        
        AppointmentType__c apt1= new AppointmentType__c();
        apt1.Name='app14';
        apt1.Category__c=cat.id;
        apt1.Price__c=100;
        apt1.Maximum_Participant__c=10;
        insert apt1;
        
          Location_Category_Mapping__c locmap=new Location_Category_Mapping__c();
        locmap.Location__c=loc.id;
        locmap.Category__c=cat.id;
        locmap.Contact__c=con.id;
        insert locmap;
        
        Event e = new Event();
        e.Subject = 'Test Event';
        e.Location = 'Test Location';
        e.Description = 'Test Description'; 
        e.StartDateTime = Datetimenow;
        e.EndDateTime = EndDt;
        e.Appointment_Type__c=apt.id;
        e.Event_Type__c='Appointment';
        e.WhoId=con.id;
        e.Location_Category_Mapping__c=locmap.id;
        e.IsRecurrence=false;
        insert e;
        
        Event e3 = new Event();
        e3.Subject = 'Test Event';
        e3.Location = 'Test Location';
        e3.Description = 'Test Description'; 
        e3.StartDateTime = Datetimenow;
        e3.EndDateTime = EndDt;
        e3.Appointment_Type__c=apt1.id;
        e3.Event_Type__c='Appointment';
        e3.WhoId=con.id;
        e3.Location_Category_Mapping__c=locmap.id;
        e3.IsRecurrence=false;
        insert e3;
        
        Event e1 = new Event();
        e1.Subject = 'Test Event';
        e1.Description = 'Test Description'; 
        e1.StartDateTime = Datetimenow;
        e1.EndDateTime = EndDt;
        e1.Appointment_Type__c=apt.id;
        e1.Event_Type__c='Cancelled';
        e1.WhoId=con.id;
        e1.Location_Category_Mapping__c=locmap.id;
        e1.IsRecurrence=false;
        insert e1;
        
        Event e2 = new Event();
        e2.Subject = 'Test Event';
        e2.Description = 'Test Description'; 
        e2.StartDateTime = Datetimenow;
        e2.EndDateTime = EndDt;
        e2.Appointment_Type__c=null;
        e2.Event_Type__c='Unavailable';
        e2.WhoId=con.id;
        e2.Location_Category_Mapping__c=locmap.id;
        e2.IsRecurrence=false;
        insert e2;
        
        EventRelation er = new EventRelation();
                er.EventId =e.id;
                er.RelationId = con1.Id; 
                er.IsParent=true;
            insert er;
        EventRelation er1 = new EventRelation();
                er1.EventId =e1.id;
                er1.RelationId = con1.Id; 
                er1.IsParent=true;
            insert er1;
       /* Event app=[Select ID,whoid,Location_Category_Mapping__r.Location__r.Name,Appointment_Type__r.name,Event_Type__c,
                                     StartDateTime,EndDateTime,(select id,relationid,eventid from eventrelations where relationid!=:Con.ID) from Event]; 
        datetime startdate=System.now();*/
        test.startTest();
        list<ExportSPAppointmentsController.Appointmentwrapper> listAppointmentwrapper=new list<ExportSPAppointmentsController.Appointmentwrapper>();
        list<ExportSPAppointmentsController.Appointmentwrapper> listcancelledAppointmentwrapper=new list<ExportSPAppointmentsController.Appointmentwrapper>();
        ExportSPAppointmentsController getapp=new ExportSPAppointmentsController();
        getapp.ContactID=con.Id;
        listAppointmentwrapper=getapp.getAppointmentwrapperlist();
        listcancelledAppointmentwrapper=getapp.getCancelledAppointmentwrapperlist();
       // ExportSPAppointmentsController.Appointmentwrapper ap =new ExportSPAppointmentsController.Appointmentwrapper(e);
        //ExportSPAppointmentsController.Appointmentwrapper ap1 =new ExportSPAppointmentsController.Appointmentwrapper(app);
       // getapp.getAppointmentslist();
        test.stopTest();
        System.assertNotEquals(null, listAppointmentwrapper);
        System.assertNotEquals(null, listcancelledAppointmentwrapper);
    }
}