public class serviceProviderCalendar
{
    public Integer noOfResRequired;
    public string serviceProviderId;
    public string resourceTypeId; 
    public AvailabilityController workingTimeSlot {get;set;}
    public integer requiredResourceCount {get;set;}  
    public map<Integer,availableMatrix> calendar; //for entire week
    boolean removeValueFromMatrix=true;
    boolean addValueToMatrix=false;  
/*----------------------------------------------------------------------------------------------------------------------------------------------- */  
    public serviceProviderCalendar(){ 
        workingTimeSlot = new AvailabilityController();
        this.noOfResRequired = 1;
        calendar = new map<Integer,availableMatrix>();
        initialiseDefaultCalnder();
    }
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/  
    public serviceProviderCalendar(Contact ServiceProvider)
    {}
    public void initialiseDefaultCalnder()
    { 
        for(integer itr=fetchCalendarUtils.forLoopStartIndex; itr<=fetchCalendarUtils.forLoopEndIndex;itr++) //for week
        {
            availableMatrix availableMatrixObj = new availableMatrix();
            availableMatrixObj.isAllDayFull = false;
            availableMatrixObj.dayCode = itr;
            availableMatrixObj.timeSlotMatrix = Utils.initMatrix(availableMatrixObj.timeSlotMatrix,fetchCalendarUtils.dayStartTime,fetchCalendarUtils.dayEndTime,0);
            calendar.put(itr,availableMatrixObj);
        }   
    } 
/*----------------------------------------------------------------------------------------------------------------------------------------------   */  
    public void getAvailTimeSlotMatix(Contact serviceProvider)
    {
        workingTimeSlot.populateTimeSlotMap(serviceProvider);
        integer isFull=8;
        system.debug('');
        for(integer itr=fetchCalendarUtils.forLoopStartIndex;itr<=fetchCalendarUtils.forLoopEndIndex;itr++) //for week
        {
            if(calendar.containsKey(itr))
            {
                availableMatrix availableMatrixObj = calendar.get(itr);
                //for(Integer j : availableMatrixObj.timeSlotMatrix.KeySet())
                //  system.debug('--serviceProviderCalendar : availableMatrixObj --'+j+'-- valu--'+availableMatrixObj.timeSlotMatrix.get(j));
                system.debug('workingTimeSlot.TimeSlots---'+workingTimeSlot.TimeSlots +'--for '+serviceProvider);
                if(workingTimeSlot.TimeSlots.containsKey(itr))
                {
                    list<AvailabilityController.timeWrapper> timeWrapLst = workingTimeSlot.TimeSlots.get(itr);
                    system.debug('init time slot timeWrapLst'+timeWrapLst);
                    if(timeWrapLst!=null && !(timeWrapLst.isEmpty()))
                    {
                        availableMatrixObj.isTimeSlotPresent = true;
                        AvailabilityController.timeWrapper timeWrapperObj = new AvailabilityController.timeWrapper();
                        AvailabilityController.timeWrapper timeWrapperObj1 = new AvailabilityController.timeWrapper();
                        timeWrapperObj = timeWrapLst.get(0);
                        integer startTime = Utils.getMilitaryTime(timeWrapperObj.startTime,false);
                        integer endTime = Utils.getMilitaryTime(timeWrapperObj.endTime,true);                
                        system.debug('starttime dr'+startTime + '   EndTime '+endTime);
                        availableMatrixObj.timeSlotMatrix = Utils.addRemoveValueFromMatrix(availableMatrixObj.timeSlotMatrix,startTime,endTime,1,addValueToMatrix); 
                        if(timeWrapLst.size()>=2)
                        {
                            timeWrapperObj1 = timeWrapLst.get(1);
                            if(timeWrapperObj1!=null)
                            {   
                                integer startTime1 = Utils.getMilitaryTime(timeWrapperObj1.startTime,false);
                                integer endTime1 = Utils.getMilitaryTime(timeWrapperObj1.endTime,true);
                                //system.debug('new startdate time '+startTime1 + ' endTime1 '+);
                                if(startTime!=startTime1 && endTime!=endTime1)
                                    availableMatrixObj.timeSlotMatrix = Utils.addRemoveValueFromMatrix(availableMatrixObj.timeSlotMatrix,startTime1,endTime1,1,addValueToMatrix);
                            }
                        }                 
                    }
                    else
                    {
                        availableMatrixObj.isTimeSlotPresent = false;
                    }         
                    //for(Integer i : availableMatrixObj.timeSlotMatrix.KeySet())
                    //system.debug('--serviceProviderCalendar : availableMatrixObj --'+i+'-- valu--'+availableMatrixObj.timeSlotMatrix.get(i));
                }
            } 
         }
         //system.debug('--serviceProviderCalendar : getAvailTimeSlotMatix : calendar'+calendar);
    }
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/
public void markUnavailableTimeSlot(list<Event> appointments)
    {
        system.debug('serviceProviderCalendar : markUnavailableTimeSlot : appointments got'+appointments + 'FOR Service Provider' + this.serviceProviderId);
        if(appointments!=null && !appointments.isEmpty())
        {
            for(Event E : appointments)
            {
                if(E.StartDateTime.format('u')=='1' && E.EndDateTime.format('u')=='1')
                    processBookedAppointment(1,E);
                else if(E.StartDateTime.format('u')=='2' && E.EndDateTime.format('u')=='2')
                    processBookedAppointment(2,E);
                else if(E.StartDateTime.format('u')=='3' && E.EndDateTime.format('u')=='3')
                    processBookedAppointment(3,E);
                else if(E.StartDateTime.format('u')=='4' && E.EndDateTime.format('u')=='4')
                    processBookedAppointment(4,E);         
                else if(E.StartDateTime.format('u')=='5' && E.EndDateTime.format('u')=='5')
                    processBookedAppointment(5,E);
                else if(E.StartDateTime.format('u')=='6' && E.EndDateTime.format('u')=='6')
                    processBookedAppointment(6,E);
                else if(E.StartDateTime.format('u')=='7' && E.EndDateTime.format('u')=='7')
                    processBookedAppointment(7,E);
            }
        }         
    }
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/
    public void processBookedAppointment(Integer dayCode,Event E)
    {
         if(calendar.containsKey(dayCode))
         {
            availableMatrix availableMatrixObj = calendar.get(dayCode);
            Integer startTime;
            Integer endTime;
            if(E.IsAllDayEvent)
            {
                startTime = 0; 
                endTime = 2345;
            }
            else
            {
                startTime = Utils.getMilitaryTime(E.StartDateTime.format('HH:mm'),false); 
                endTime = Utils.getMilitaryTime(E.EndDateTime.format('HH:mm'),true);
            }
            system.debug('*** processBookedAppointment : startTime ***'+startTime);
            system.debug('*** processBookedAppointment : endTime ***'+endTime);        
            availableMatrixObj.timeSlotMatrix = Utils.addRemoveValueFromMatrix(availableMatrixObj.timeSlotMatrix,startTime,endTime,1,removeValueFromMatrix); //0 for mark those slot as unavailable    
         }
    }
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/  
}