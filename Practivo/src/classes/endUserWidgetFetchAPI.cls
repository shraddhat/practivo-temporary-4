@RestResource(urlMapping='/Appointed/endUserWidget/fetch') 
global class endUserWidgetFetchAPI{
    
    /*  Author : Darshan Chhajed.
        Discription : This method is used for enduser widget. This method get data for 4 picklist (Location , Service Category , Appointment Type, Service Provider)
                      Also this return Metadata for calendar. 
    */
    @HttpGet
    global static void getEndUserWidgetLeftPane()
    {
        Utils.isCallComingFromAPI = true;    
        Utils.retriveLocationHelper returnObject= new Utils.retriveLocationHelper();
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.addHeader('Access-Control-Allow-Origin', '*');
        RestContext.response.addHeader('Access-Control-Allow-Methods', '*');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(returnObject));
 
    }
    /*  Author : Darshan Chhajed.
        Discription : This method is used for enduser widget. This method return morning and evening slot for month if
        called with Fetch_Monthly_Calendar value for Fetch_Method  and return Available slot for one day if called with Fetch_Daily_Available_TimeSlot. 
        In Month we just returing flag for morning and evening slot to show colored dot in User Widget. In Daily call we are returing time slot where end user can
        book appointment in selected date. For class we are only fetching class that are already craeted and has capacity to occupy more participant.  
    */
    @HttpPost
    global static void getEndUserWidgetMonthlyCalendarAndEvents()    
    {
        string Fetch_Monthly_Calendar = 'Fetch_Monthly_Calendar';
        string Fetch_Daily_Available_TimeSlot = 'Fetch_Daily_Available_TimeSlot';
        
        string returnValue='ERROR:Some Error Has Occuered At Server';
        try{
            
            RestRequest httpRequest = RestContext.request;
            RestResponse httpResponse = RestContext.response;
            map<string,string> fetchDataMap =  new map<String,string>(); 
            for(string str :  RestContext.request.params.KeySet())
            {
                fetchDataMap.put(str, RestContext.request.params.get(str));
            }
            system.debug('** endUserWidgetAPI : getEndUserWidgetMonthlyCalendarAndEvents : Request Data **'+ fetchDataMap);
            boolean isClassAppointment,isAnyPractitioner;
            if(fetchDatamap.containsKey('isClass'))
                isClassAppointment = Boolean.valueOf(fetchDataMap.get('isClass'));
            if(fetchDataMap.get('selectPractitionerfornewapp')=='Any')
                isAnyPractitioner = true;
            string condition ='  WHERE Appointment_Type__c= '+'\''+String.escapeSingleQuotes(fetchDataMap.get('selectatfornewapp'))+'\'' ;  
            list<Resource_Type_Appointment_Type_Mapping__c> resTyeAppTypLst = DatabaseQueries.getAppTypResTypMapping(condition); 
            system.debug('**** endUserWidgetAPI : getEndUserWidgetMonthlyCalendarAndEvents  :resTyeAppTypLst**'+resTyeAppTypLst);
            /*LOGIC FOR FETCHING EVENTS FOR ENTIRE MONTH*/
            if(fetchDatamap.containsKey('Fetch_Method') && fetchDataMap.get('Fetch_Method')==Fetch_Monthly_Calendar)
            {
                
                if(resTyeAppTypLst.size()>0)
                {
                    fetchCalendarUtils fetchCalendarObject = new fetchCalendarUtils(); //Don't Construct Resources Inventory
                    Date calendarStartDate = fetchCalendarObject.getDate(fetchDataMap.get('appointmentDate'));
                    Integer numberDays = Date.daysInMonth(calendarStartDate.year(), calendarStartDate.month());
                    Date calendarEndDate = calendarStartDate.toStartOfMonth().addDays(numberDays-1);
                    //calendarStartDate = calendarStartDate.toStartOfWeek(); 
                    fetchCalendarObject.initialiseForEnduserWidget(calendarStartDate,Integer.ValueOf(resTyeAppTypLst.get(0).Appointment_Type__r.DefaultDuration__c));  
                    system.debug('*** endUserWidgetAPI : getEndUserWidgetMonthlyCalendarAndEvents : calendarStartDate'+calendarStartDate);
                    system.debug('*** endUserWidgetAPI : getEndUserWidgetMonthlyCalendarAndEvents : calendarEndDate'+calendarEndDate);
                    
                    fetchCalendarObject.getForStartNEnd(1,7);//for entrire Week
                    fetchCalendarObject.populateLocationCatMappingLsts(fetchDataMap.get('selectLocationfornewapp'),fetchDataMap.get('selectPractitionerfornewapp'));
                    
                    map<Id,Integer> humanResourceRequirements = fetchCalendarObject.getHumanResourceRequirement(resTyeAppTypLst);
                    system.debug('**** endUserWidgetAPI : getEndUserWidgetMonthlyCalendarAndEvents  : humanResourceRequirements **'+humanResourceRequirements);
                    map<Id,Integer> nonHumanResourceRequirments = fetchCalendarObject.getNonHumanResourceRequirement(resTyeAppTypLst);
                    system.debug('-**** endUserWidgetAPI : getEndUserWidgetMonthlyCalendarAndEvents  : nonHumanResourceRequirments--'+nonHumanResourceRequirments);
                    
                    fetchCalendarObject.populateServiceProvidersNAssistants(humanResourceRequirements.KeySet()); //get all Service Provider  and assistamnts in the system    
                    map<Id,list<Contact>> resourceTypeWithAllAssistantsMap = new map<Id,list<Contact>>();
                    resourceTypeWithAllAssistantsMap = fetchCalendarObject.getResTypAssistantMap(humanResourceRequirements.KeySet());
                    map<Id,list<Resources__c>> resourceTypeToActualResMap = new map<Id,list<Resources__c>>();
                    resourceTypeToActualResMap = fetchCalendarObject.getResourceTypeWithResourcesMap(nonHumanResourceRequirments.KeySet(),fetchDataMap.get('selectLocationfornewapp'));       
                    
                    if(!isClassAppointment)/*ONE On ONE APPOINTMENT*/
                    {
                        fetchCalendarObject.getEventRecurringDaily(fetchDataMap.get('selectPractitionerfornewapp'),calendarStartDate,calendarEndDate);
                    }
                    else /*CLASS APPOINTMENT*/
                    {
                        integer size = fetchCalendarObject.getAllPartialClassAppointment(fetchDataMap.get('selectLocationfornewapp'),fetchDataMap.get('selectatfornewapp'),fetchDataMap.get('selectPractitionerfornewapp'),fetchDataMap.get('selectCategoryfornewapp'),calendarStartDate,calendarEndDate);
                    }
                    set<Id> idSet = fetchCalendarObject.getAllBookedResource();
                    list<Actual_Resource_Booked_Mapping__c> aRBMlst = fetchCalendarObject.getAllBookedResourcesMappings(idSet);
                    fetchCalendarObject.addActualResourceMappingLstForRecurring(aRBMlst);
                    
                    Integer adderNumber=0;
                    Date startDate =calendarStartDate;
                    Time tym  = Time.newInstance(0,0,0,0);
                    DateTime sT= DateTime.newInstanceGMT(startDate,tym); 
                    Date endDate = startDate.addDays(7-Integer.ValueOf(sT.format('u')));
                    
                    if(endDate>calendarEndDate) 
                        endDate = calendarEndDate;     
                    system.debug('*** endUserWidgetAPI : getEndUserWidgetMonthlyCalendarAndEvents  : startDate *'+startDate);
                    system.debug('**** endUserWidgetAPI : getEndUserWidgetMonthlyCalendarAndEvents  : endDate *'+endDate);
                    list<Contact> serviceProviderLst= new list<Contact>();
                    set<Id> serviceProviderIdSet = new set<id>();
                    set<Id> resourceTypeServiceProvidersSet = new set<id>();
                    if(boolean.Valueof(fetchDataMap.get('isAnyPractitioner'))==true)
                    {
                            serviceProviderLst = fetchCalendarObject.serviceProviders.Values();
                            for(Contact SP : serviceProviderLst)
                            {
                                resourceTypeServiceProvidersSet.add(SP.Resource_Type__c);
                            }
                            serviceProviderIdSet = fetchCalendarObject.serviceProviders.KeySet();
                    }
                    else
                    {
                        Contact serviceProvider = fetchCalendarObject.serviceProviders.get(fetchDataMap.get('selectPractitionerfornewapp'));  
                        serviceProviderLst.add(serviceProvider);
                        serviceProviderIdSet.add(serviceProvider.Id);
                    }
                    while(calendarEndDate>=endDate)
                    {
                        tym  = Time.newInstance(0,0,0,0);
                        DateTime startDateTime= DateTime.newInstanceGMT(startDate,tym); 
                        DateTime endDateTime  = DateTime.newInstanceGMT(endDate,tym);
                        system.debug('**StartIndex -'+startDateTime.format('u') + ' EndIndex-**' +endDateTime.format('u'));
                        
                        fetchCalendarObject.getForStartNEnd(Integer.valueOf(startDateTime.format('u')),Integer.valueOf(endDateTime.format('u')));
                        fetchCalendarObject.allWeekEvents = fetchCalendarObject.getEventsForDateRantge(startDate,endDate);
                        if(!isClassAppointment)
                        {
                            if(boolean.Valueof(fetchDataMap.get('isAnyPractitioner'))==true)
                                fetchCalendarObject.getWorkingTimeNAppointments(resourceTypeServiceProvidersSet,false,false);
                            else
                                fetchCalendarObject.getWorkingTimeNAppointments(serviceProviderIdSet,true,false);
                            fetchCalendarObject.populateServiceProviderCalendarLst(serviceProviderLst,true);
                            fetchCalendarObject.nonHumanResourceDataSetLst = fetchCalendarObject.getnonServiceProviderDataSetForDateRange(startDate,endDate);
                            system.debug('-- Current nonHumanResourceDataSetLst--'+  fetchCalendarObject.nonHumanResourceDataSetLst);
                            fetchCalendarObject.getAllNonHumanResourcesCalendar(nonHumanResourceRequirments,resourceTypeToActualResMap);    
                            fetchCalendarObject.getAllNonSPHumanResourceCalendar(humanResourceRequirements,resourceTypeWithAllAssistantsMap);
                            fetchCalendarObject.getFinalCalendarWithAllSP();
                            fetchCalendarObject.getFinalCalendarWithAlNonSP(); 
                        }
                        else
                        {
                            fetchCalendarObject.getCalnedarForClassAppointment();
                        }
                        fetchCalendarObject.getEventForAPI(1); //1 for monthly events
                        if(endDate==calendarEndDate)
                                break;
                        startDate =  endDate.addDays(1);
                        system.debug('**saveRecurringAppointments1 : startDate After Calculation'+startDate);
                        endDate = startDate.addDays(6);
                        system.debug('**saveRecurringAppointments1 : endDate After Calculation'+endDate);
                        if(calendarEndDate<endDate)
                        {
                            endDate = calendarEndDate;
                        }
                    }
                    returnValue = JSON.serialize(fetchCalendarObject.monthlyEventsMap);
                }
                else
                {
                    returnValue = 'Error : No Appointment Type Resource Type Mapping Found.';   
                }
                system.debug('--endUserWidgetAPI : getEndUserWidgetMonthlyCalendarAndEvents : Fetch_Monthly_Calendar :  returnValue--'+returnValue);    
            }
            
            /* LOGIC FOR FETCHING ALLAVAILABLE TIMINGS IN SELECTED DAY*/
            else if(fetchDatamap.containsKey('Fetch_Method') && fetchDataMap.get('Fetch_Method')==Fetch_Daily_Available_TimeSlot)
            {
                fetchCalendarUtils fetchCalendarObject = new fetchCalendarUtils();
                
                Date det = fetchCalendarObject.getDate(fetchDataMap.get('appointmentDate'));
                fetchCalendarObject.initialiseForEnduserWidget(det,Integer.ValueOf(resTyeAppTypLst.get(0).Appointment_Type__r.DefaultDuration__c));
                Time tym  = Time.newInstance(0,0,0,0);
                DateTime t= DateTime.newInstanceGMT(det,tym);
                fetchCalendarObject.getForStartNEnd(Integer.valueof(t.format('u')),Integer.valueof(t.format('u')));//for entrire Day 
                fetchCalendarObject.getDateRange(det,det);
                system.debug('--fetchDataMap.value ----'+fetchDataMap.get('isAnyPractitioner'));//+ 'Value' +boolean.Valueof(fetchDataMap.get('isAnyPractitioner')));
                 if(isClassAppointment)
                 {
                        fetchCalendarObject.getAllPartialClassAppointment(fetchDataMap.get('selectLocationfornewapp'),fetchDataMap.get('selectatfornewapp'),fetchDataMap.get('selectPractitionerfornewapp'),fetchDataMap.get('selectCategoryfornewapp'),det,det);   
                        fetchCalendarObject.allWeekEvents = fetchCalendarObject.getEventsForDateRantge(det,det);
                        fetchCalendarObject.getCalnedarForClassAppointment();
				 }
                 else
                 {
	                    if(boolean.Valueof(fetchDataMap.get('isAnyPractitioner'))==true)
	                    {           
	                        fetchCalendarObject.getAllWeeksEvents(fetchDataMap.get('selectLocationfornewapp'),'Any');
	                        fetchCalendarObject.processAppTypResTyp(resTyeAppTypLst,null,Id.valueOf(fetchDataMap.get('selectLocationfornewapp')),Id.valueOf(fetchDataMap.get('selectCategoryfornewapp')));
	                    }
	                    else if(boolean.Valueof(fetchDataMap.get('isAnyPractitioner'))==false)
	                    { 
	                        fetchCalendarObject.getAllWeeksEvents(fetchDataMap.get('selectLocationfornewapp'),fetchDataMap.get('selectPractitionerfornewapp'));
	                        fetchCalendarObject.processAppTypResTyp(resTyeAppTypLst,Id.valueOf(fetchDataMap.get('selectPractitionerfornewapp')),Id.valueOf(fetchDataMap.get('selectLocationfornewapp')),Id.valueOf(fetchDataMap.get('selectCategoryfornewapp')));
	                    }
	                    fetchCalendarObject.getFinalCalendarWithAllSP();
	                    fetchCalendarObject.getFinalCalendarWithAlNonSP();
	             }  
                 fetchCalendarObject.getEventForAPI(2); //2 for daily events
                 returnValue = JSON.serialize(fetchCalendarObject.dailyEventWrapperObj);
                 
            }
            httpResponse.addHeader('Content-Type', 'application/json');
            httpResponse.addHeader('Access-Control-Allow-Origin', '*');
            httpResponse.addHeader('Access-Control-Allow-Methods', '*');  
            httpResponse.responseBody = Blob.valueOf(returnValue); 
        }catch(Exception e)
        {
            system.debug('Exception e'+e.getStackTraceString()+'Messgae--'+e.getMessage());
        }
       
                 
    }
}