public class workingTimeSlotScheduler implements Schedulable {
    public void execute(SchedulableContext SC) {
        
        list<event> availTimeSlots = new list<event>(); 
        list<contact> serviceProviderLst = new list<Contact>();
        User us;
        try
        {
            if(Utils.getReadAccessCheck('USER',new String []{'Id','profileId','isAdmin__c'}))
                us=[SELECT Id,Email,ProfileId FROM User WHERE Profile.Name='System Administrator' LIMIT 1];
            else
                throw new Utils.ParsingException('No Read access to USER or Fields in USER.');
            AvailabilityController availCntrOb = new AvailabilityController();
            if(Utils.getReadAccessCheck('Contact',new String []{'Id','Name','Email','firstname','lastname','Phone','Birthdate','RecordTypeId','PractitionerColor__c','Salutation','Resource_Type__c'}))
            	serviceProviderLst = [SELECT Id, Name,Email,firstname,lastname,Phone,Birthdate,RecordTypeId,PractitionerColor__c,Salutation,Start_Date_To_Generate_Calendar__c,End_Date_To_Generate_Calendar__c,Resource_Type__r.Id FROM Contact WHERE RecordType.developername =:Utils.contact_ServiceProviderRecordType LIMIT 2000];	
            else
            	throw new Utils.ParsingException('No Read access to Contact or Fields in Contact.');
            serviceProviderLst = availCntrOb.fetchAllServiceProvider(); 
            availCntrOb.fetchAllAvailableTimeSlot(true);    //For All Service Peovider
             availCntrOb.isBulkInsertion=true ;
            for(Contact ServiceProvider : serviceProviderLst)
            {
                availCntrOb.processTimeSlot(ServiceProvider);
                availCntrOb.saveServiceProviderWorkingTimeSlot();           
            }
            availCntrOb.deleteAllFreeSlot(null,null,false);
            System.debug(availCntrOb.saveAllTimeSlots());
            if(Utils.getCreateAccessCheck('Event', new String []{}))
            	Database.insert(availTimeSlots);
        	else 
        	 throw new Utils.ParsingException('No Create Access on Event.');	
        }catch(Exception e)
        {
            system.debug('==>   Exception : workingTimeSlotScheduler :'+e.getMessage()+'    Trace==>'+e.getStackTraceString());
            SendNotificationEmails.sendEmailForExportCalendar(us.Id, us.Id,false, e.getMessage(),e.getStackTraceString());
        }
}
}