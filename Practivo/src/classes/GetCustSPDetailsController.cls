/*
***************************************
 class name-GetCustSPDetailsController
 Class Description-Controller for VF components Cmp_SendCustomerDetailsForClass and Cmp_SendCustomerDetails,cmp_sendserviceproviderdetails,Cmp_SendCustomerDetailsForEmptyClass 
 *************************************
*/
public with sharing class GetCustSPDetailsController 
{
public ID EventID{get{return EventID;}set;} 
public ID EventwhoID{get{return EventwhoID;}set;}
public ID EventwhatID{get{return EventwhatID;}set;}
public list<event> listevents=new list<event>();
public list<eventrelation> listeventrelations=new list<eventrelation>();
public string Custset_Serviceprovider{get;set;}
public string Custset_Customer{get;set;}
public String Custset_Category{get;set;}
public String Custset_Location{get;set;}
public String strDateTimeofEvent{get;set;} //for start datetime in locale dateformat
public String endDateTimeofEvent{get;set;} //for end datetime in locale dateformat
Set <id> setofrelids=new set<id>();
list<contact> listcontact=new list<contact>();
contact ServiceProvider=new contact();
Public Event originalEvent=new event();

public GetCustSPDetailsController()
{
  //custom settings for dynamic labels for practitioner,category,location and patient
Configuration_Labels__c LabelChangerSettingsCategory= Configuration_Labels__c.getInstance('Category');
Configuration_Labels__c LabelChangerSettingsLocation= Configuration_Labels__c.getInstance('Location');
Configuration_Labels__c LabelChangerSettingsServiceProvider= Configuration_Labels__c.getInstance('Service Provider');
Configuration_Labels__c LabelChangerSettingsCustomer= Configuration_Labels__c.getInstance('Customer');
Custset_Location=String.valueof(LabelChangerSettingsLocation.value__c); 
Custset_Serviceprovider=String.valueof(LabelChangerSettingsServiceProvider.value__c);
Custset_Customer=String.valueof(LabelChangerSettingsCustomer.value__c);
Custset_Category=String.valueof(LabelChangerSettingsCategory.value__c);
}
public event getevent()
{
  try
  {
    if(Utils.getReadAccessCheck('Event',new String []{'Id','Subject','whatid','Capacity__c', 'StartDateTime','EndDateTime','WhoId','No_of_Participants__c', 'Event_Type__c','Location_Category_Mapping__c','Appointment_Type__c','Description__c'}))
      originalEvent=[SELECT Id,Subject,Capacity__c, StartDateTime, EndDateTime, WhoId, WhatId,Appointment_Type__r.name, Appointment_Type__c, Appointment_Type__r.Maximum_Participant__c , Description__c, Event_Type__c, Location_Category_Mapping__c,Location_Category_Mapping__r.Location__r.name,Location_Category_Mapping__r.Category__r.name,No_of_Participants__c FROM Event WHERE ID=:String.escapeSingleQuotes(EventID) LIMIT 1];
    else
      throw new Utils.ParsingException('No Read access to Event or Fields in Event.');  
      Datetime d1=Datetime.valueOf(originalEvent.StartDateTime);
      Datetime d2=Datetime.valueOf(originalEvent.EndDateTime);
      strDateTimeofEvent=d1.format();
      endDateTimeofEvent=d2.format();
  }  
  catch(exception e)
  {
    system.debug('GetCustSPDetailsController-->getevent-->'+e.getmessage());
  }
  return originalEvent;
}
public list<contact> getCustomerslist()
    {
      try
      {
        if(Utils.getReadAccessCheck('eventrelation',new String []{'Id','relationid','eventid'}))
          listeventrelations=[SELECT id,relationid,eventid FROM eventrelation WHERE (relationid !=:String.escapeSingleQuotes(EventwhoID) AND relationid !=:String.escapeSingleQuotes(EventwhatID)) AND eventid=:String.escapeSingleQuotes(EventID) LIMIT 2000];
        else
           throw new Utils.ParsingException('No Read access to eventrelation or Fields in eventrelation.');
         if(!listeventrelations.isEmpty())
         {
             for(eventrelation er:listeventrelations)
             {
                 setofrelids.add(String.escapeSingleQuotes(er.relationid));
             }
         if(Utils.getReadAccessCheck('contact',new String []{'Id','name','email','Phone'}))    
          listcontact=[SELECT id,name,email,Phone FROM contact WHERE id in:setofrelids LIMIT 2000];
         else
           throw new Utils.ParsingException('No Read access to contact or Fields in contact.');
         } 
       }
       catch(exception e)
       {
        system.debug('GetCustSPDetailsController-->getCustomerslist-->'+e.getmessage());
       }
        return listcontact;
    }
public contact getServiceProvider()
    {
         try
         {
          if(Utils.getReadAccessCheck('contact',new String []{'Id','name','email','Phone'}))
            ServiceProvider=[SELECT id,name,email,Phone FROM contact WHERE id=:String.escapeSingleQuotes(EventwhoID) LIMIT 2000]; 
          else
           throw new Utils.ParsingException('No Read access to contact or Fields in contact.');  
         }
         catch(exception e)
         {
         }
         return ServiceProvider;
    }    
}