trigger PreventAppointmentTypeDeleteTrigger on AppointmentType__c (before Delete) 
{
	Set<ID> ApptypeidSet = New Set<ID>();
    List<Event> eventList = [SELECT Id,StartDateTime, EndDateTime, WhoId, WhatId, Appointment_Type__c,Event_Type__c, Location_Category_Mapping__c FROM Event WHERE Event_Type__c=:Utils.EventType_Appointment OR Event_Type__c=:Utils.EventType_Partial];
    for(Event evrecord : eventList)
    {
        ApptypeidSet.add(evrecord.Appointment_Type__c);
    }
   
    For(AppointmentType__c Apptyperecord : trigger.old){
        
        If(ApptypeidSet.contains(Apptyperecord.ID))
            Apptyperecord.addError('You cannot delete this record, as appointment exists in a system for this appointment type.');
    }
}