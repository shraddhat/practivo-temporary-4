/*
Details : Updates the count of the total specific resources and total overbooking limit of those specific resources on Resource category record when new
          specific resource gets added/updated or when you delete any.
*/
trigger updateResourceCountTrigger on Resources__c (after insert,after update,before delete) 
{
    try
    {
        map<id,List<Resources__c>> restyperesourceMap=new map<id,List<Resources__c>>();
        map<id,List<Resources__c>> restyperesourceMapOld=new map<id,List<Resources__c>>();
        if (Trigger.isBefore) 
        {
            if (Trigger.isDelete) 
            {
                for (Resources__c res : Trigger.old) 
                {
                    list<Resources__c> lst;
                            if(restyperesourceMap.get(res.Resource_Type__c)==null)
                            {
                                lst = new list<Resources__c>();
                                lst.add(res);
                                restyperesourceMap.put(res.Resource_Type__c,lst);
                            }
                            else
                                restyperesourceMap.get(res.Resource_Type__c).add(res);
                } 
                List<Resource_Type__c> RtypeList=[SELECT Id,Name,Type_of_Resource__c,Total_No_of_Resources__c,Total_Overbooking_Limit__c FROM Resource_Type__c WHERE Id IN:restyperesourceMap.keySet()];
                for(Resource_Type__c rtyp:RtypeList)
                {
                    list<Resources__c> lst;
                    Decimal count=0;
                   if(restyperesourceMap.containsKey(rtyp.Id))
                   {
                       lst=restyperesourceMap.get(rtyp.Id);
                       for(Resources__c rs:lst)
                       {
                          count=count+rs.Over_Booking_Limit__c;
                       }
                      rtyp.Total_No_of_Resources__c=rtyp.Total_No_of_Resources__c-lst.size();
                      rtyp.Total_Overbooking_Limit__c=rtyp.Total_Overbooking_Limit__c-count;    
                       System.debug('rtyp.Total_No_of_Resources__c--'+rtyp.Total_No_of_Resources__c);
                   }
                }
                UPDATE RtypeList;
            }
        }
        else if(Trigger.isAfter)
        {
            if (Trigger.isInsert) 
            {
                for (Resources__c res : Trigger.new) 
                {
                    list<Resources__c> lst;
                            if(restyperesourceMap.get(res.Resource_Type__c)==null)
                            {
                                lst = new list<Resources__c>();
                                lst.add(res);
                                restyperesourceMap.put(res.Resource_Type__c,lst);
                            }
                            else
                                restyperesourceMap.get(res.Resource_Type__c).add(res);
                } 
                List<Resource_Type__c> RtypeList=[SELECT Id,Name,Type_of_Resource__c,Total_No_of_Resources__c,Total_Overbooking_Limit__c FROM Resource_Type__c WHERE Id IN:restyperesourceMap.keySet()];
                for(Resource_Type__c rtyp:RtypeList)
                {
                    list<Resources__c> lst;
                    Decimal count=0;
                   if(restyperesourceMap.containsKey(rtyp.Id))
                   {
                       lst=restyperesourceMap.get(rtyp.Id);
                       for(Resources__c rs:lst)
                       {
                          count=count+rs.Over_Booking_Limit__c;
                       }
                      rtyp.Total_No_of_Resources__c=rtyp.Total_No_of_Resources__c+lst.size();
                      rtyp.Total_Overbooking_Limit__c=rtyp.Total_Overbooking_Limit__c+count;    
                       System.debug('rtyp.Total_No_of_Resources__c--'+rtyp.Total_No_of_Resources__c);
                   }
                }
                UPDATE RtypeList;
            }
            else if(Trigger.isUpdate)
            {
                for (Resources__c res : Trigger.new) 
                {
                    list<Resources__c> lst;
                            if(restyperesourceMap.get(res.Resource_Type__c)==null)
                            {
                                lst = new list<Resources__c>();
                                lst.add(res);
                                restyperesourceMap.put(res.Resource_Type__c,lst);
                            }
                            else
                                restyperesourceMap.get(res.Resource_Type__c).add(res);
                } 
                
                for (Resources__c res : Trigger.old) 
                {
                    list<Resources__c> lst;
                            if(restyperesourceMapOld.get(res.Resource_Type__c)==null)
                            {
                                lst = new list<Resources__c>();
                                lst.add(res);
                                restyperesourceMapOld.put(res.Resource_Type__c,lst);
                            }
                            else
                                restyperesourceMapOld.get(res.Resource_Type__c).add(res);
                } 
                
                List<Resource_Type__c> RtypeList=[SELECT Id,Name,Type_of_Resource__c,Total_No_of_Resources__c,Total_Overbooking_Limit__c FROM Resource_Type__c WHERE Id IN:restyperesourceMap.keySet()];
                for(Resource_Type__c rtyp:RtypeList)
                {
                    list<Resources__c> lst;
                    list<Resources__c> lstold;
                    Decimal count=0;
                    Decimal countold=0;
                    Decimal Diff;
                   if(restyperesourceMap.containsKey(rtyp.Id))
                   {
                       lst=restyperesourceMap.get(rtyp.Id);
                       lstold=restyperesourceMapOld.get(rtyp.Id);
                       for(Resources__c rs:lst)
                       {
                          count=count+rs.Over_Booking_Limit__c;
                       }
                       for(Resources__c rs:lstold)
                       {
                          countold=countold+rs.Over_Booking_Limit__c;
                       }
                        if(countOld>count)
                      { 
                          Diff=countOld-count;
                          rtyp.Total_Overbooking_Limit__c=rtyp.Total_Overbooking_Limit__c-Diff;  
                      }
                      else if(count>countOld)
                      {
                         Diff=count-countOld;
                          rtyp.Total_Overbooking_Limit__c=rtyp.Total_Overbooking_Limit__c+Diff; 
                      }
                      
                   }
                }
                UPDATE RtypeList;
            }
        }
  }  
    catch(Exception e)
    {
        System.debug('updateResourceCountTriger-->'+e.getMessage()+e.getStackTraceString());
    }    
}